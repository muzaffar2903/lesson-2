function printDistinct(arr, n) {
  // Pick all elements one by one
  for (let i = 0; i < n; i++) {
    // Check if the picked element is already printed
    var j;
    for (j = 0; j < i; j++) if (arr[i] == arr[j]) break;

    // If not printed earlier, then print it
    if (i == j) console.log(arr[i] + " ");
  }
}

// Driver program to test above function
arr = new Array(6, 10, 5, 4, 9, 120, 4, 6, 10);
n = arr.length;
printDistinct(arr, n);
